@extends('layout')

@section('title', 'Форма кандидата')

@section('content')

    <form method="post" class="form-horizontal">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Имя:</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Имечко" required>
        </div>

        <div class="mb-3">
            <label for="surname" class="form-label">Фамилия:</label>
            <input type="text" class="form-control" id="surname" name="surname" placeholder="Фамилечка" required>
        </div>

        <div class="mb-3">
            <label for="email" class="form-label">Email:</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Мыло" required>
        </div>

        <div class="mb-3">
            <label for="age" class="form-label">Возраст:</label>
            <input type="number" max="100" min="16" class="form-control" id="age" name="age" placeholder="Сколько?" required>
        </div>

        <div class="mb-3">
            <label for="sex" class="form-label">Пол:</label>
            <select id="sex" name="sex_id" class="form-control" required>
                <option disabled selected>Выберите пол</option>
                @foreach($sexes as $sex)
                    <option value="{{ $sex->id }}">{{ $sex->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="city" class="col-sm-2 control-label">Город:</label>
            <select id="city" name="city_id" class="form-control" required>
                <option disabled selected>Выберите город</option>
                @foreach($cities as $city)
                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <button type="submit" class="btn btn-primary">Отправить</button>
        </div>
    </form>

@endsection
