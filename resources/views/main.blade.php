@extends('layout')

@section('title', 'Список кандидатов')

@section('content')

    <br>
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Мыло</th>
                <th scope="col">Возраст</th>
                <th scope="col">Пол</th>
                <th scope="col">Город</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($candidates as $candidate)
                <tr>
                    <td>{{ $candidate->id }}</td>
                    <td>{{ $candidate->name }}</td>
                    <td>{{ $candidate->surname }}</td>
                    <td>{{ $candidate->email }}</td>
                    <td>{{ $candidate->age }}</td>
                    <td>{{ $candidate->sex->name }}</td>
                    <td>{{ $candidate->city->name }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
