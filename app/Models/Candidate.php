<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;
    protected $table = 'candidates';
    protected $fillable = [
        'name',
        'surname',
        'email',
        'age',
        'sex_id',
        'city_id',
    ];
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function sex()
    {
        return $this->belongsTo(Sex::class);
    }
    public $timestamps = false;
}
