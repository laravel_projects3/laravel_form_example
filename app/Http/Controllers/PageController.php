<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\City;
use App\Models\Sex;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function formPage()
    {
        $cities = City::all();
        $sexes = Sex::all();
        return view('form', compact('cities', 'sexes'));
    }
    public function mainPage()
    {
        $candidates = Candidate::all();
        return view('main', compact('candidates'));
    }

}
