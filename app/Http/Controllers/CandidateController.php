<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function saveData(Request $request)
    {

        try {
            $validated = $request->validate([
                'name' => 'required|max:100|min:2',
                'surname' => 'required|max:100|min:2',
                'email' => 'required|email||max:100',
                'age' => 'required|integer|max:100',
                'sex_id' => 'required|integer',
                'city_id' => 'required|integer',
            ]);

            Candidate::create($validated);

            return redirect('/')->with('success', 'Запись успешно сохранена');
        } catch (\Exception $e) {
           return redirect('/')->with('error', $e);
        }
    }
}
