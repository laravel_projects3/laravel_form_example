<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sexes')->insert([
            ['name' => 'женский'],
            ['name' => 'мужской']
        ]);
        DB::table('cities')->insert([
            ['name' => 'Ижевск'],
            ['name' => 'Уфа'],
            ['name' => 'Пермь'],
            ['name' => 'Краснодар'],
            ['name' => 'Новосибирск'],
            ['name' => 'Светлогорск'],
            ['name' => 'Москва'],
            ['name' => 'Астрахань'],
        ]);
    }
}
